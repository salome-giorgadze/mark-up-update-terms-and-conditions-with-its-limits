
function confirmRules() {
    if (document.getElementById("checkbox").checked) {
        document.querySelector(".cookies_rules_conf_btn").disabled = false
    } else {
        document.querySelector(".cookies_rules_conf_btn").disabled = true
    }
}
document.getElementById("checkbox").addEventListener("click", confirmRules);